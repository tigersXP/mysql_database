DROP DATABASE IF EXISTS weather_data;
create database weather_data;
use weather_data;

create table sensor_readings
(	PackageNumber INT auto_increment not null
	,Device varchar(40) not null
    ,Temp float not NULL
    ,Humitity float not NULL
    ,Pressure float not NULL
    ,Luminosity float not NULL
    ,Time int(8) not NULL
    , constraint pk_Temperature primary key (PackageNumber)
);

